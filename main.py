#!/usr/bin/env python2
# -*- coding: utf-8; mode: python -*-
""" Python 2.7+ / 3.4+ script to measure the level of coffee in a coffee pot.

- Date:    27/01/2017.
- Author:  Lilian Besson, (C) 2017
- Licence: MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
from time import time, sleep
import numpy as np

# From http://softwarerecs.stackexchange.com/a/18135
try:
    from SimpleCV import Camera
except:
    print("SimpleCV seems to not be available. Have you installed it?")
    Camera = None  # Make sphinx doc work


# from skimage.filter.rank import entropy
# from skimage.morphology import disk

# Local module import
from FreeSMS import main as sendsms

# Parameters
title = "WebCam Python Coffee Level detector"
duration_measure = 5  # In seconds
duration_loop = 10    # In seconds
sleeptime = None


def img2graynp(img):
    """Converts a SimpleCV image to a numpy array of floats ni [0, 1]. """
    # Get black/white numpy array
    # mat = np.array(img.getGrayNumpy(), dtype=float)
    mat = np.array(img.binarize().getGrayNumpy(), dtype=float)
    mat /= max(np.max(mat), 1)  # Go to [0, 1]
    return mat


def measure(empty=False, sleeptime=sleeptime, duration=duration_measure):
    """Ask the user to present an empty/full pot, measure the level and print it and return it."""
    message = "{} pot, please".format("Empty" if empty else "Full")
    print(message)
    print("  (Waiting for", duration, "seconds ...)")
    # Initialize the camera
    cam = Camera()
    start = time()
    # Loop to continuously get images
    while True:
        # Get Image from camera
        img = cam.getImage()
        mean = np.mean(img2graynp(img))
        # Draw the text "Hello World" on image
        img.drawText("{}. {}. Mean pixel = {:g}".format(title, message, mean), 50, 50, fontsize=22)
        # Show the image
        img.show()
        if sleeptime:
            sleep(sleeptime)
        if time() - start > duration:
            break
    print("Measured mean pixel value :", mean)
    return mean


def loop(mini, maxi, sleeptime=sleeptime, duration=duration_loop):
    """Main program with known mini/maxi pixel count for empty/full pot."""
    print("  (Waiting for", duration, "seconds ...)")
    # Initialize the camera
    cam = Camera()
    start = time()
    # Loop to continuously get images
    while True:
        # Get Image from camera
        img = cam.getImage()
        mean = np.mean(img2graynp(img))
        level = (mean - mini) / maxi  # Linear interpolation
        # Draw the text "Hello World" on image
        img.drawText("{}. Level = {:.2%}".format(title, level), 50, 50, fontsize=22)
        # Show the image
        img.show()
        # img2 = entropy(img2graynp(img), disk(5))
        # img2.show()
        if sleeptime:
            sleep(sleeptime)
        if time() - start > duration:
            break
        if level < 0 or level > 1:
            print("Error: level of coffee should be between 0% and 100% only. Maybe you measured it wrongly? Exiting...")
            break
        elif level < 0.2:
            print("The coffee pot is almost empty!")
        else:
            print("There is still coffee...")
    return level


template_sms = """The level of the coffee pot is apparently {level:.2%}.

{msg}"""


def main():
    """Main program: measures empty and full pot, then launch the loop."""
    mini = measure(empty=True)
    maxi = measure(empty=False)
    level = loop(mini, maxi)
    if level < 0.2:
        msg = "The coffee pot is almost empty! Do some more!"
    else:
        msg = "There is still coffee..."
    sendsms(template_sms.format(level=level, msg=msg))


if __name__ == '__main__':
    main()

# End of main.py
