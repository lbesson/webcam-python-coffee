.. WebCam Python - Coffee Level Tracker documentation master file, created by
   sphinx-quickstart on Fri Jan 27 14:30:35 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WebCam Python - Coffee Level Tracker's documentation!
================================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   main
   README.md
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
