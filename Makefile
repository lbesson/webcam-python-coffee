# Makefile for https://bitbucket.org/lbesson/webcam-python-coffee

# Using bash and not sh, cf. http://stackoverflow.com/a/589300/
SHELL := /bin/bash -o pipefail

# Help and test to run and know how to run the script
help:
	pydoc main
test:
	python main.py

# Installers
install:
	pip install -r requirements.txt
getFreeSMS:
	wget 'https://bitbucket.org/lbesson/bin/raw/master/FreeSMS.py'

# Linters and doc
lint:
	pylint main.py | tee main.pylint.txt
lint3:
	pylint --py3k main.py | tee main.py3lint.txt
doc:
	pydoc main | tee main.pydoc.txt

# Cleaner
clean:
	\rm -vfr *.pyc *~ __pycache__/

# Minimal makefile for Sphinx documentation
# You can set these variables from the command line.
PYTHON        = python
SPHINXOPTS    =
# SPHINXBUILD   = /usr/local/bin/sphinx-build
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = WebCamPython-CoffeeLevelTracker
SOURCEDIR     = .
BUILDDIR      = _build

# Put it first so that "make" without argument is like "make help".
helpsphinx:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

clean-doc:
	mkdir --parents $(BUILDDIR)/html
	-rm -rfv /tmp/$(BUILDDIR)/
	mv -vf $(BUILDDIR)/ /tmp/
	mkdir --parents $(BUILDDIR)/html/

.PHONY: help

apidoc:
	# -mkdir -vf /tmp/webcam-python-coffee.git/docs/
	# -mv -vf docs/*.rst /tmp/webcam-python-coffee.git/docs/
	@echo "==> Showing you which .rst files will be created in this directory : $(pwd)"
	sphinx-apidoc -n -o . -e -M .
	@echo "==> OK to generate these files ? [Enter for OK, Ctrl+C to cancel]"
	@read
	sphinx-apidoc -o . -e -M .

html:
	$(SPHINXBUILD) -M html "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	@echo "Build finished. The HTML pages are in $(BUILDDIR)/html."
